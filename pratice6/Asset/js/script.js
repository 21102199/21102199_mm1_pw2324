
function getQuotes(url,title) {
    var item = $("#quote-item");
    $("#title-page").html(title);
    $(".quote-item").remove();
    item.appendTo("#quote-items");
    $.ajax({
    type : 'GET',
    url: url,
    beforeSend: function( xhr ) {
    xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
    },
    error: function(xhr, status, error) {
    console.log(xhr);
    }
    })
    .done(function(data){
        myData = JSON.parse(data);
        for(i=0; i < myData['quotes'].length;i++){
            var item = $("#quote-item").clone();
        item.find("#card-description").html(myData["quotes"][i].description);
        item.removeAttr("style");
        item.find("#card-img").attr("src", myData["quotes"][i].images);
        item.find("#card-title").html(myData["quotes"][i].title);
        item.find("#card-name").html(myData["quotes"][i].name + " (" + myData["quotes"][i].class + ")");
        item.find("#card-date").html(myData["quotes"][i].updated);
        item.find("#viewdetail").attr("onClick", "callModal('" + myData["quotes"][i].title + "','" + myData["quotes"][i].name + " (" + myData["quotes"][i].class + ")','" + myData["quotes"][i].images + "','" + myData["quotes"][i].description + "')");
        item.appendTo("#quote-items");
        }
    });
}
    $(document).ready(function(){
        getQuotes('https://quotesbeta.tugaskelas.my.id/index.php/quotes/q/my_quote/21102199','My Quotes');
        })